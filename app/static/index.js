"use strict";

var canvas = document.querySelector("#canvas");
var context = canvas.getContext("2d");
canvas.width = 480;
canvas.height = 480;

var mouse = { x: 0, y: 0 };
var lastMouse = { x: 0, y: 0 };
context.fillStyle = "white";
context.fillRect(0, 0, canvas.width, canvas.height);
context.color = "#094074";
context.lineWidth = 28;
context.lineJoin = context.linecap = "round";

function getMousePos(canvas, e) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: e.clientX - rect.left,
    y: e.clientY - rect.top,
  };
}

canvas.addEventListener(
  "mousemove",
  (e) => {
    lastMouse.x = mouse.x;
    lastMouse.y = mouse.y;

    mouse.x = getMousePos(canvas, e).x;
    mouse.y = getMousePos(canvas, e).y;
  },
  false
);

canvas.addEventListener(
  "mousedown",
  (e) => {
    canvas.addEventListener("mousemove", paint, false);
  },
  false
);
canvas.addEventListener(
  "mouseup",
  (e) => {
    canvas.removeEventListener("mousemove", paint, false);
  },
  false
);

var paint = function () {
  context.lineWidth = context.lineWidth;
  context.lineJoin = "round";
  context.lineCap = "round";
  context.strokeStyle = context.color;

  context.beginPath();
  context.moveTo(lastMouse.x, lastMouse.y);
  context.lineTo(mouse.x, mouse.y);
  context.closePath();
  context.stroke();
};

var clearButton = $("#clearButton");
clearButton.on("click", () => {
  context.clearRect(0, 0, 280, 280);
  context.fillStyle = "white";
  context.fillRect(0, 0, canvas.width, canvas.height);
  $("#result").text("");
});
