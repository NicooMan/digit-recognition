from flask import Flask, render_template, request
import numpy as np
import re
import sys
import os
import base64
from skimage import io
from skimage.util import invert
from skimage import io
from skimage.color import rgb2gray
from skimage.transform import resize
from tensorflow.keras.models import model_from_json

# sys.path.append(os.path.abspath('./model'))

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

app = Flask(__name__)


def convertImage(imgData):
    imgstr = re.search(r'base64,(.*)', imgData.decode('utf-8')).group(1)
    return imgstr.encode('utf-8')


def processImg(img):
    processedImg = img[:, :, :3]
    processedImg = rgb2gray(processedImg)
    processedImg = resize(processedImg, (28, 28))
    processedImg = invert(processedImg)
    processedImg = processedImg.reshape(28, 28, 1)
    return processedImg


def load_model():
    with open('app/model/model.json', 'r') as m:
        loaded_model_json = m.read()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights('app/model/model.h5')
    print('##########Model loaded from disk#################')
    return loaded_model


model = load_model()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    imgData = request.get_data()
    imgstr = convertImage(imgData)
    with open('app/input.png', 'wb') as output:
        output.write(base64.decodebytes(imgstr))

    img = io.imread('app/input.png')
    input_image = processImg(img).reshape(1, 28, 28, 1)
    prediction = np.argmax(model.predict(input_image))
    print(prediction)
    return (str(prediction))


def run():
    app.run(host='0.0.0.0', port=80, debug=True, threaded=False)
